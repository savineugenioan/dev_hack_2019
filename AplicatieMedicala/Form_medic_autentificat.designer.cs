﻿namespace AplicatieMedicala
{
    partial class Form_medic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textboxOras = new System.Windows.Forms.TextBox();
            this.textboxNume = new System.Windows.Forms.TextBox();
            this.labelzona = new System.Windows.Forms.Label();
            this.labelNumeUtil2 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textboxCNP = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(93, 30);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 0;
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(93, 86);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "NUME";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "PRENUME";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 63);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(152, 82);
            this.button1.TabIndex = 5;
            this.button1.Text = "Fisa Pacient";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(82, 88);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(120, 57);
            this.button2.TabIndex = 6;
            this.button2.Text = "Cauta Medici";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textboxOras);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.textboxNume);
            this.groupBox2.Controls.Add(this.labelzona);
            this.groupBox2.Controls.Add(this.labelNumeUtil2);
            this.groupBox2.Location = new System.Drawing.Point(11, 190);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(213, 151);
            this.groupBox2.TabIndex = 18;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Cautare Medici";
            // 
            // textboxOras
            // 
            this.textboxOras.Location = new System.Drawing.Point(82, 63);
            this.textboxOras.Margin = new System.Windows.Forms.Padding(2);
            this.textboxOras.Name = "textboxOras";
            this.textboxOras.Size = new System.Drawing.Size(120, 20);
            this.textboxOras.TabIndex = 3;
            // 
            // textboxNume
            // 
            this.textboxNume.Location = new System.Drawing.Point(82, 32);
            this.textboxNume.Margin = new System.Windows.Forms.Padding(2);
            this.textboxNume.Name = "textboxNume";
            this.textboxNume.Size = new System.Drawing.Size(120, 20);
            this.textboxNume.TabIndex = 2;
            // 
            // labelzona
            // 
            this.labelzona.AutoSize = true;
            this.labelzona.Location = new System.Drawing.Point(15, 63);
            this.labelzona.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelzona.Name = "labelzona";
            this.labelzona.Size = new System.Drawing.Size(37, 13);
            this.labelzona.TabIndex = 1;
            this.labelzona.Text = "ORAS";
            // 
            // labelNumeUtil2
            // 
            this.labelNumeUtil2.AutoSize = true;
            this.labelNumeUtil2.Location = new System.Drawing.Point(15, 32);
            this.labelNumeUtil2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelNumeUtil2.Name = "labelNumeUtil2";
            this.labelNumeUtil2.Size = new System.Drawing.Size(39, 13);
            this.labelNumeUtil2.TabIndex = 0;
            this.labelNumeUtil2.Text = "NUME";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(276, 30);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(101, 76);
            this.button3.TabIndex = 19;
            this.button3.Text = "Reveniti la meniul de Login";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textboxCNP);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Location = new System.Drawing.Point(238, 190);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(174, 151);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cautare Pacient";
            // 
            // textboxCNP
            // 
            this.textboxCNP.Location = new System.Drawing.Point(38, 36);
            this.textboxCNP.Margin = new System.Windows.Forms.Padding(2);
            this.textboxCNP.Name = "textboxCNP";
            this.textboxCNP.Size = new System.Drawing.Size(120, 20);
            this.textboxCNP.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 39);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "CNP";
            // 
            // Form_medic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 360);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Name = "Form_medic";
            this.Text = "Interfata Medic";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_medic_FormClosed);
            this.Load += new System.EventHandler(this.Form_medic_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textboxOras;
        private System.Windows.Forms.TextBox textboxNume;
        private System.Windows.Forms.Label labelzona;
        private System.Windows.Forms.Label labelNumeUtil2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textboxCNP;
        private System.Windows.Forms.Label label3;
    }
}

