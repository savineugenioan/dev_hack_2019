﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace AplicatieMedicala
{
    public partial class Form_medic : Form
    {
        public OleDbCommand cmd = new OleDbCommand();
        public OleDbConnection cnn = new OleDbConnection();
        public OleDbDataReader reader;
        public Form_medic()
        {
            InitializeComponent();
        }

        private void Form_medic_Load(object sender, EventArgs e)
        {
            string connetionString = null;
            string sql = null;
            textboxNume.Text = "";
            textboxOras.Text = "";
            textboxCNP.Text = "";
            connetionString = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + AppDomain.CurrentDomain.BaseDirectory + @"Proiect_Cabinet.accdb";
            sql = @"SELECT DOCTORI.nume_doctor FROM DOCTORI WHERE (((DOCTORI.CNP) LIKE '"+ Global.str1 + "'"+" ))";
            cnn = new OleDbConnection(connetionString);
            cmd = new OleDbCommand(sql, cnn);
            try
            {
                if (cnn.State == ConnectionState.Closed)
                {
                    cnn.Open();
                }
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string x = reader.GetValue(0).ToString();
                    string[] atomi = x.Split(' ');
                    textBox1.Text = atomi[0];
                    textBox2.Text = atomi[1];
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            reader.Close();
            cmd.Dispose();
            cnn.Close();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            Global.medici.Clear();
            LastForm.last = this;
            string nume = "", oras = "";
            nume = textboxNume.Text;
            oras = textboxOras.Text;
            string connetionString = null;
            string sql = null;
            connetionString = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + AppDomain.CurrentDomain.BaseDirectory + @"Proiect_Cabinet.accdb";
            sql = @"SELECT DOCTORI.nume_doctor, SPECIALIZARE.nume_specializare, DOCTORI.nr_telefon, DOCTORI.oras, DOCTORI.rest_adresa, DOCTORI.lang_long, DOCTORI.Feedback_doctor FROM SPECIALIZARE INNER JOIN DOCTORI ON SPECIALIZARE.id_specializare = DOCTORI.specializare";
            cnn = new OleDbConnection(connetionString);
            cmd = new OleDbCommand(sql, cnn);
            try
            {
                if (cnn.State == ConnectionState.Closed)
                {
                    cnn.Open();
                }
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Medici m =new Medici(reader.GetValue(0).ToString(), reader.GetValue(1).ToString(), reader.GetValue(2).ToString(), reader.GetValue(3).ToString(), reader.GetValue(4).ToString(), reader.GetValue(5).ToString(), reader.GetValue(6).ToString());
                    Global.medici.Add(m);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            reader.Close();
            cmd.Dispose();
            cnn.Close();
            try
            {
                if (nume != "")
                    foreach (Medici med in Global.medici.ToList())
                    {
                        if (!(med.nume.Contains(nume)))
                            Global.medici.Remove(med);
                    }
                if (oras != "")
                    foreach (Medici med in Global.medici.ToList())
                    {
                        if (med.oras.ToUpper() != oras.ToUpper())
                            Global.medici.Remove(med);
                    }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            try
            {
                if (Global.medici.Count > 0)
                {
                    this.Hide();
                    Form f = new Form_Medici_Harti();
                    f.Show();
                }
                else
                    MessageBox.Show("Nu a fost gasit nici un medic conform specificatiilor introduse ! Verificati daca numele introdus incepe cu majuscula !");
            }
            catch { }
        }
        private void Form_medic_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form f1 = new c();
            f1.Show();
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form f = new c();
            f.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
                LastForm.last = this;
                Global.str1 = textboxCNP.Text;
                string connetionString = null;
                string sql = null;
                connetionString = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + AppDomain.CurrentDomain.BaseDirectory + @"Proiect_Cabinet.accdb";
                sql = @"SELECT PACIENTI.CNP FROM PACIENTI";
                cnn = new OleDbConnection(connetionString);
                cmd = new OleDbCommand(sql, cnn);
                try
                {
                    if (cnn.State == ConnectionState.Closed)
                    {
                        cnn.Open();
                    }
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        string x = textboxCNP.Text;
                        string y = reader.GetValue(0).ToString();
                        if (x == y)
                        {
                            this.Hide();
                            Form f = new Form_Fisa_Pacient();
                            f.Show();
                            return;
                        }
                    }   
                            MessageBox.Show("Pacientul nu exista !");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                reader.Close();
                cmd.Dispose();
                cnn.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }
    }
    public class Medici
    {
        public string nume, specializare, telefon, oras, rest_adresa, langlong = "",feedback="0";
        public Medici(string nume, string specializare, string telefon, string oras, string rest_adresa, string langlong,string feedback="0")
        {
            this.nume = nume;
            this.specializare = specializare;
            this.telefon = telefon;
            this.oras = oras;
            this.rest_adresa = rest_adresa;
            this.feedback = feedback;
            this.langlong = langlong;
        }
        public int CompareTo(Medici a)
        {
            return (int.Parse(a.feedback).CompareTo(int.Parse(this.feedback)));
        }
        public static int Compare(Medici a,Medici b)
        {
            return (int.Parse(b.feedback).CompareTo(int.Parse(a.feedback)));
        }
    }

}
