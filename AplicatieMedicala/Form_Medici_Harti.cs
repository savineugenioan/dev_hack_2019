﻿using GMap.NET.MapProviders;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AplicatieMedicala
{
    public partial class Form_Medici_Harti : Form
    {
        Medici m;
        int i = 0;
        string url = @"https://maps.google.com/maps?q=";
        public Form_Medici_Harti()
        {
            InitializeComponent();
        }

        private void Form_Medici_Harti_Load(object sender, EventArgs e)
        {
            Global.medici.Sort(Medici.Compare);
            m = Global.medici[i];
            afisare();
            if (Global.medici.IndexOf(m) == 0)
                button1.Enabled = false;
            else
                button1.Enabled = true;
            if (Global.medici.IndexOf(m) == Global.medici.Count-1)
                button2.Enabled = false;
            else
                button2.Enabled = true;
        }

        private void Form_Medici_Harti_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            i -= 1;
            m = Global.medici[i];
            afisare();
            if (Global.medici.IndexOf(m) == 0)
                button1.Enabled = false;
            else
                button1.Enabled = true;
            if (Global.medici.IndexOf(m) == Global.medici.Count - 1)
                button2.Enabled = false;
            else
                button2.Enabled = true; ;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            i++;
            m = Global.medici[i];
            afisare();
            if (Global.medici.IndexOf(m) == 0)
                button1.Enabled = false;
            else
                button1.Enabled = true;
            if (Global.medici.IndexOf(m) == Global.medici.Count - 1)
                button2.Enabled = false;
            else
                button2.Enabled = true;
        }
        public void afisare()
        {
            try
            {
                gMapControl1.Overlays.Clear();
                string s;
                textBox1.Text = m.nume;
                textBox2.Text = m.specializare;
                textBox3.Text = m.oras;
                textBox4.Text = m.rest_adresa;
                textBox5.Text = m.telefon;
                textBox6.Text = m.feedback + "/5";
                s = url + m.langlong;
                gMapControl1.MapProvider = GMapProviders.BingMap;
                gMapControl1.Position = new GMap.NET.PointLatLng(double.Parse(m.langlong.Substring(0, m.langlong.IndexOf(','))),
                    double.Parse(m.langlong.Substring(m.langlong.IndexOf(',')+2)));
                GMap.NET.WindowsForms.GMapOverlay markers = new GMap.NET.WindowsForms.GMapOverlay("markers");
                GMap.NET.WindowsForms.GMapMarker marker =
                    new GMap.NET.WindowsForms.Markers.GMarkerGoogle(
                        new GMap.NET.PointLatLng(double.Parse(m.langlong.Substring(0, m.langlong.IndexOf(','))),
                    double.Parse(m.langlong.Substring(m.langlong.IndexOf(',') + 2))),
                        GMap.NET.WindowsForms.Markers.GMarkerGoogleType.blue_pushpin);
                markers.Markers.Add(marker);
                gMapControl1.Overlays.Add(markers);
                gMapControl1.MinZoom = 5;
                gMapControl1.MaxZoom = 20;
                gMapControl1.Zoom = 18;
            }
            catch
            {

            }

        }
        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            LastForm.last.Show();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
    public static class LastForm
    {
        public static Form last;
    }
}
