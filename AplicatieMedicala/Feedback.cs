﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace AplicatieMedicala
{
    public partial class Feedback : Form
    {
        public OleDbCommand cmd = new OleDbCommand();
        public OleDbConnection cnn = new OleDbConnection();
        public OleDbDataReader reader;
        public Feedback()
        {
            InitializeComponent();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            string id_doctor="";
            bool b = false;
            string connetionString = null;
            string sql = null;
            connetionString = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + AppDomain.CurrentDomain.BaseDirectory + @"Proiect_Cabinet.accdb";
            cnn = new OleDbConnection(connetionString);
            try
            {
                if (cnn.State == ConnectionState.Closed)
                {
                    cnn.Open();
                }
                sql = @"SELECT CONSULT.id_doctor FROM CONSULT WHERE (((CONSULT.id_consult) Like '" + textBoxConsultatie.Text + "'))";
                cmd = new OleDbCommand(sql, cnn);
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    id_doctor = reader.GetValue(0).ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            try
            {
                if (cnn.State == ConnectionState.Closed)
                {
                    cnn.Open();
                }
                
                    sql = @"SELECT CONSULT.id_consult FROM PACIENTI INNER JOIN CONSULT ON PACIENTI.id_client = CONSULT.id_client WHERE(((PACIENTI.CNP)Like '"+Global.str1+"'))";
                    cmd = new OleDbCommand(sql, cnn);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                    if (reader.GetValue(0).ToString() == textBoxConsultatie.Text)
                    {
                        b = true;
                        break;
                    }
                    }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            if (b == false)
                MessageBox.Show("Id Consultatie gresit !");
            else
            {
                double x = 0;
                if (radioButton1.Checked == true)
                    x = x + 1;

                else if (radioButton2.Checked == true)
                    x = x + 2;
                else if (radioButton3.Checked == true)
                    x = x + 3;
                else if (radioButton4.Checked == true)
                    x = x + 4;
                else if (radioButton5.Checked == true)
                    x = x + 5;
                if (radioButton10.Checked == true)
                    x = x + 1;
                else if (radioButton9.Checked == true)
                    x = x + 2;
                else if (radioButton8.Checked == true)
                    x = x + 3;
                else if (radioButton7.Checked == true)
                    x = x + 4;
                else if (radioButton6.Checked == true)
                    x = x + 5;
                if (radioButton15.Checked == true)
                    x = x + 1;
                else if (radioButton14.Checked == true)
                    x = x + 2;
                else if (radioButton13.Checked == true)
                    x = x + 3;
                else if (radioButton12.Checked == true)
                    x = x + 4;
                else if (radioButton11.Checked == true)
                    x = x + 5;

                textBox2.Text = (x / 3).ToString();
                int z = 0;
                int g = 0;
                if (textBox2.Text != null & textBoxConsultatie.Text != null)
                {
                    connetionString = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + AppDomain.CurrentDomain.BaseDirectory + @"Proiect_Cabinet.accdb";

                    cnn = new OleDbConnection(connetionString);

                    try
                    {
                        if (cnn.State == ConnectionState.Closed)
                        {
                            cnn.Open();
                        }

                        try
                        {
                            sql = @"UPDATE CONSULT SET CONSULT.Feedback_consult = " + textBox2.Text + " WHERE (((CONSULT.id_consult)Like '" + textBoxConsultatie.Text + "'" + " ))";

                            cmd = new OleDbCommand(sql, cnn);
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                        finally
                        {
                            MessageBox.Show("Feedback primit !");
                        }

                        cmd.Dispose();
                        cnn.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }


                    try
                    {
                        if (cnn.State == ConnectionState.Closed)
                        {
                            cnn.Open();
                        }
                        try
                        {
                            //sql = @"SELECT CONSULT.id_doctor, Sum(CONSULT.Feedback_consult) AS SumOfFeedback_consult FROM CONSULT GROUP BY CONSULT.id_doctor HAVING (((CONSULT.id_doctor) Like '" + textBoxConsultatie.Text + "'" + " ));";
                            sql = @"SELECT Sum(CONSULT.Feedback_consult) AS SumOfFeedback_consult FROM CONSULT GROUP BY CONSULT.id_doctor HAVING (((CONSULT.id_doctor) Like "+id_doctor+"))";

                            cmd = new OleDbCommand(sql, cnn);
                            reader = cmd.ExecuteReader();
                            while (reader.Read())
                            {
                                z = int.Parse(reader.GetValue(0).ToString());
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                        finally
                        {
                            //MessageBox.Show("Feedback primit !");
                        }

                        cmd.Dispose();
                        cnn.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                    try
                    {
                        if (cnn.State == ConnectionState.Closed)
                        {
                            cnn.Open();
                        }
                        try
                        {
                            sql = @"SELECT Count(CONSULT.id_consult) AS CountOfid_consult, CONSULT.id_doctor FROM CONSULT GROUP BY CONSULT.id_doctor HAVING (((CONSULT.id_doctor) Like '" + id_doctor + "'" + "));";


                            cmd = new OleDbCommand(sql, cnn);
                            reader = cmd.ExecuteReader();
                            while (reader.Read())
                            { g = int.Parse(reader.GetValue(0).ToString()); }

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                        finally
                        {
                            //MessageBox.Show("Feedback primit !");
                        }

                        cmd.Dispose();
                        cnn.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    try
                    {
                        if (cnn.State == ConnectionState.Closed)
                        {
                            cnn.Open();
                        }

                        try
                        {
                            double n = (double)z / g;
                            sql = "UPDATE DOCTORI SET DOCTORI.Feedback_doctor = " + n + " WHERE (((DOCTORI.id_doctor) Like " + id_doctor + "))";
                            cmd = new OleDbCommand(sql, cnn);
                            cmd.ExecuteNonQuery();



                        }
                        catch (DivideByZeroException ex)
                        {
                            double m = 0;

                            sql = @"UPDATE DOCTORI SET DOCTORI.Feedback_doctor = " + m + " WHERE (((DOCTOR.id_doctor)Like '" + textBoxConsultatie.Text + "'" + " ))";

                            cmd = new OleDbCommand(sql, cnn);
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                        finally
                        {
                            //MessageBox.Show("Feedback primit !");
                        }

                        cmd.Dispose();
                        cnn.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void Feedback_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            Form f = new FormPacient();
            f.Show();
        }

        private void Feedback_Load(object sender, EventArgs e)
        {
            string CNP = Global.str1;

        }
    }
    }

