﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace AplicatieMedicala
{
    public partial class FormPacient : Form
    {
        public OleDbCommand cmd = new OleDbCommand();
        public OleDbConnection cnn = new OleDbConnection();
        public OleDbDataReader reader;

        public FormPacient()
        {
            InitializeComponent();
        }
        private void button2_Click_1(object sender, EventArgs e)
        {
            Global.medici.Clear();
            LastForm.last = this;
            string nume = "", oras = "";
            nume = textboxNume.Text;
            oras = textboxOras.Text;
            string connetionString = null;
            string sql = null;
            connetionString = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + AppDomain.CurrentDomain.BaseDirectory + @"Proiect_Cabinet.accdb";
            sql = @"SELECT DOCTORI.nume_doctor, SPECIALIZARE.nume_specializare, DOCTORI.nr_telefon, DOCTORI.oras, DOCTORI.rest_adresa, DOCTORI.lang_long, DOCTORI.Feedback_doctor FROM SPECIALIZARE INNER JOIN DOCTORI ON SPECIALIZARE.id_specializare = DOCTORI.specializare";
            cnn = new OleDbConnection(connetionString);
            cmd = new OleDbCommand(sql, cnn);
            try
            {
                if (cnn.State == ConnectionState.Closed)
                {
                    cnn.Open();
                }
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Medici m = new Medici(reader.GetValue(0).ToString(), reader.GetValue(1).ToString(), reader.GetValue(2).ToString(), reader.GetValue(3).ToString(), reader.GetValue(4).ToString(), reader.GetValue(5).ToString(), reader.GetValue(6).ToString());
                    Global.medici.Add(m);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            reader.Close();
            cmd.Dispose();
            cnn.Close();
            try
            {
                if (nume != "")
                    foreach (Medici med in Global.medici.ToList())
                    {
                        if (!(med.nume.Contains(nume)))
                            Global.medici.Remove(med);
                    }
                if (oras != "")
                    foreach (Medici med in Global.medici.ToList())
                    {
                        if (med.oras.ToUpper() != oras.ToUpper())
                            Global.medici.Remove(med);
                    }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            try
            {
                if (Global.medici.Count > 0)
                {
                    this.Hide();
                    Form f = new Form_Medici_Harti();
                    f.Show();
                }
                else
                    MessageBox.Show("Nu a fost gasit nici un medic conform specificatiilor introduse ! Verificati daca numele introdus incepe cu majuscula !");
            }
            catch { }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LastForm.last = this;
            if (Global.str1.Length == 13)
            {
                string connetionString = null;
                string sql = null;
                connetionString = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + AppDomain.CurrentDomain.BaseDirectory + @"Proiect_Cabinet.accdb";
                sql = @"SELECT PACIENTI.CNP FROM PACIENTI";
                cnn = new OleDbConnection(connetionString);
                cmd = new OleDbCommand(sql, cnn);
                try
                {
                    if (cnn.State == ConnectionState.Closed)
                    {
                        cnn.Open();
                    }
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        string x = Global.str1;
                        string y = reader.GetValue(0).ToString();
                        if (x == y)
                        {
                            this.Hide();
                            Form f = new Form_Fisa_Pacient();
                            f.Show();
                            return;
                        }
                    }
                    MessageBox.Show("Pacientul nu exista !");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                reader.Close();
                cmd.Dispose();
                cnn.Close();
            }
            else
            {
                MessageBox.Show("CNP gresit !");
            }
        }

        private void FormPacient_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void button_FeedBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form f = new Feedback();
            f.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form f = new c();
            f.Show();
        }

        private void FormPacient_Load(object sender, EventArgs e)
        {

        }
    }
}