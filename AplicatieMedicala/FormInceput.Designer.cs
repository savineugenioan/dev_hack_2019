﻿namespace AplicatieMedicala
{
    partial class c
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonAutentificare = new System.Windows.Forms.Button();
            this.ButtonAdaugare = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.labelNume = new System.Windows.Forms.Label();
            this.labelprenume = new System.Windows.Forms.Label();
            this.labelCNP = new System.Windows.Forms.Label();
            this.labelAdresa = new System.Windows.Forms.Label();
            this.labelTelefon = new System.Windows.Forms.Label();
            this.textBoxNume = new System.Windows.Forms.TextBox();
            this.textBoxPrenume = new System.Windows.Forms.TextBox();
            this.textBoxCNP = new System.Windows.Forms.TextBox();
            this.textBoxAdresa = new System.Windows.Forms.TextBox();
            this.textBoxTelefon = new System.Windows.Forms.TextBox();
            this.labelEmail = new System.Windows.Forms.Label();
            this.textBoxEmail = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxConfirmareP = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxParola = new System.Windows.Forms.TextBox();
            this.labelParola = new System.Windows.Forms.Label();
            this.buttonInregistrare = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonIncepe = new System.Windows.Forms.Button();
            this.textBoxParola2 = new System.Windows.Forms.TextBox();
            this.textBoxNumeUtil2 = new System.Windows.Forms.TextBox();
            this.labelParola2 = new System.Windows.Forms.Label();
            this.labelNumeUtil2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // ButtonAutentificare
            // 
            this.ButtonAutentificare.Location = new System.Drawing.Point(94, 55);
            this.ButtonAutentificare.Margin = new System.Windows.Forms.Padding(2);
            this.ButtonAutentificare.Name = "ButtonAutentificare";
            this.ButtonAutentificare.Size = new System.Drawing.Size(196, 44);
            this.ButtonAutentificare.TabIndex = 0;
            this.ButtonAutentificare.Text = "AUTENTIFICARE";
            this.ButtonAutentificare.UseVisualStyleBackColor = true;
            this.ButtonAutentificare.Click += new System.EventHandler(this.ButtonAutentificare_Click);
            // 
            // ButtonAdaugare
            // 
            this.ButtonAdaugare.Location = new System.Drawing.Point(472, 55);
            this.ButtonAdaugare.Margin = new System.Windows.Forms.Padding(2);
            this.ButtonAdaugare.Name = "ButtonAdaugare";
            this.ButtonAdaugare.Size = new System.Drawing.Size(196, 44);
            this.ButtonAdaugare.TabIndex = 1;
            this.ButtonAdaugare.Text = "ADAUGARE CONT";
            this.ButtonAdaugare.UseVisualStyleBackColor = true;
            this.ButtonAdaugare.Click += new System.EventHandler(this.ButtonAdaugare_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "PACIENT",
            "MEDIC"});
            this.comboBox1.Location = new System.Drawing.Point(18, 26);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(141, 21);
            this.comboBox1.TabIndex = 2;
            // 
            // labelNume
            // 
            this.labelNume.AutoSize = true;
            this.labelNume.Location = new System.Drawing.Point(16, 23);
            this.labelNume.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelNume.Name = "labelNume";
            this.labelNume.Size = new System.Drawing.Size(39, 13);
            this.labelNume.TabIndex = 4;
            this.labelNume.Text = "NUME";
            // 
            // labelprenume
            // 
            this.labelprenume.AutoSize = true;
            this.labelprenume.Location = new System.Drawing.Point(16, 54);
            this.labelprenume.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelprenume.Name = "labelprenume";
            this.labelprenume.Size = new System.Drawing.Size(61, 13);
            this.labelprenume.TabIndex = 5;
            this.labelprenume.Text = "PRENUME";
            // 
            // labelCNP
            // 
            this.labelCNP.AutoSize = true;
            this.labelCNP.Location = new System.Drawing.Point(16, 179);
            this.labelCNP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelCNP.Name = "labelCNP";
            this.labelCNP.Size = new System.Drawing.Size(29, 13);
            this.labelCNP.TabIndex = 6;
            this.labelCNP.Text = "CNP";
            // 
            // labelAdresa
            // 
            this.labelAdresa.AutoSize = true;
            this.labelAdresa.Location = new System.Drawing.Point(16, 84);
            this.labelAdresa.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelAdresa.Name = "labelAdresa";
            this.labelAdresa.Size = new System.Drawing.Size(51, 13);
            this.labelAdresa.TabIndex = 7;
            this.labelAdresa.Text = "ADRESA";
            // 
            // labelTelefon
            // 
            this.labelTelefon.AutoSize = true;
            this.labelTelefon.Location = new System.Drawing.Point(16, 113);
            this.labelTelefon.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelTelefon.Name = "labelTelefon";
            this.labelTelefon.Size = new System.Drawing.Size(56, 13);
            this.labelTelefon.TabIndex = 8;
            this.labelTelefon.Text = "TELEFON";
            // 
            // textBoxNume
            // 
            this.textBoxNume.Location = new System.Drawing.Point(142, 20);
            this.textBoxNume.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxNume.Name = "textBoxNume";
            this.textBoxNume.Size = new System.Drawing.Size(125, 20);
            this.textBoxNume.TabIndex = 9;
            // 
            // textBoxPrenume
            // 
            this.textBoxPrenume.Location = new System.Drawing.Point(142, 52);
            this.textBoxPrenume.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxPrenume.Name = "textBoxPrenume";
            this.textBoxPrenume.Size = new System.Drawing.Size(125, 20);
            this.textBoxPrenume.TabIndex = 10;
            // 
            // textBoxCNP
            // 
            this.textBoxCNP.Location = new System.Drawing.Point(142, 172);
            this.textBoxCNP.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxCNP.Name = "textBoxCNP";
            this.textBoxCNP.Size = new System.Drawing.Size(125, 20);
            this.textBoxCNP.TabIndex = 11;
            // 
            // textBoxAdresa
            // 
            this.textBoxAdresa.Location = new System.Drawing.Point(142, 84);
            this.textBoxAdresa.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxAdresa.Name = "textBoxAdresa";
            this.textBoxAdresa.Size = new System.Drawing.Size(125, 20);
            this.textBoxAdresa.TabIndex = 12;
            // 
            // textBoxTelefon
            // 
            this.textBoxTelefon.Location = new System.Drawing.Point(142, 109);
            this.textBoxTelefon.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxTelefon.Name = "textBoxTelefon";
            this.textBoxTelefon.Size = new System.Drawing.Size(125, 20);
            this.textBoxTelefon.TabIndex = 13;
            // 
            // labelEmail
            // 
            this.labelEmail.AutoSize = true;
            this.labelEmail.Location = new System.Drawing.Point(16, 143);
            this.labelEmail.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelEmail.Name = "labelEmail";
            this.labelEmail.Size = new System.Drawing.Size(39, 13);
            this.labelEmail.TabIndex = 14;
            this.labelEmail.Text = "EMAIL";
            // 
            // textBoxEmail
            // 
            this.textBoxEmail.Location = new System.Drawing.Point(142, 141);
            this.textBoxEmail.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxEmail.Name = "textBoxEmail";
            this.textBoxEmail.Size = new System.Drawing.Size(125, 20);
            this.textBoxEmail.TabIndex = 15;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxConfirmareP);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBoxParola);
            this.groupBox1.Controls.Add(this.labelParola);
            this.groupBox1.Controls.Add(this.buttonInregistrare);
            this.groupBox1.Controls.Add(this.textBoxNume);
            this.groupBox1.Controls.Add(this.labelCNP);
            this.groupBox1.Controls.Add(this.textBoxCNP);
            this.groupBox1.Controls.Add(this.labelNume);
            this.groupBox1.Controls.Add(this.labelprenume);
            this.groupBox1.Controls.Add(this.textBoxEmail);
            this.groupBox1.Controls.Add(this.labelEmail);
            this.groupBox1.Controls.Add(this.labelAdresa);
            this.groupBox1.Controls.Add(this.textBoxTelefon);
            this.groupBox1.Controls.Add(this.labelTelefon);
            this.groupBox1.Controls.Add(this.textBoxAdresa);
            this.groupBox1.Controls.Add(this.textBoxPrenume);
            this.groupBox1.Location = new System.Drawing.Point(472, 156);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(304, 315);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Inregistrare Pacienti";
            this.groupBox1.Visible = false;
            // 
            // textBoxConfirmareP
            // 
            this.textBoxConfirmareP.Location = new System.Drawing.Point(143, 231);
            this.textBoxConfirmareP.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxConfirmareP.Name = "textBoxConfirmareP";
            this.textBoxConfirmareP.PasswordChar = '*';
            this.textBoxConfirmareP.Size = new System.Drawing.Size(125, 20);
            this.textBoxConfirmareP.TabIndex = 22;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 238);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "CONFIRMATI PAROLA";
            // 
            // textBoxParola
            // 
            this.textBoxParola.Location = new System.Drawing.Point(142, 202);
            this.textBoxParola.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxParola.Name = "textBoxParola";
            this.textBoxParola.PasswordChar = '*';
            this.textBoxParola.Size = new System.Drawing.Size(125, 20);
            this.textBoxParola.TabIndex = 20;
            // 
            // labelParola
            // 
            this.labelParola.AutoSize = true;
            this.labelParola.Location = new System.Drawing.Point(16, 209);
            this.labelParola.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelParola.Name = "labelParola";
            this.labelParola.Size = new System.Drawing.Size(50, 13);
            this.labelParola.TabIndex = 18;
            this.labelParola.Text = "PAROLA";
            // 
            // buttonInregistrare
            // 
            this.buttonInregistrare.Location = new System.Drawing.Point(140, 287);
            this.buttonInregistrare.Margin = new System.Windows.Forms.Padding(2);
            this.buttonInregistrare.Name = "buttonInregistrare";
            this.buttonInregistrare.Size = new System.Drawing.Size(127, 24);
            this.buttonInregistrare.TabIndex = 16;
            this.buttonInregistrare.Text = "INREGISTRARE";
            this.buttonInregistrare.UseVisualStyleBackColor = true;
            this.buttonInregistrare.Click += new System.EventHandler(this.buttonInregistrare_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonIncepe);
            this.groupBox2.Controls.Add(this.textBoxParola2);
            this.groupBox2.Controls.Add(this.comboBox1);
            this.groupBox2.Controls.Add(this.textBoxNumeUtil2);
            this.groupBox2.Controls.Add(this.labelParola2);
            this.groupBox2.Controls.Add(this.labelNumeUtil2);
            this.groupBox2.Location = new System.Drawing.Point(94, 176);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(279, 205);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Autentificare";
            this.groupBox2.Visible = false;
            // 
            // buttonIncepe
            // 
            this.buttonIncepe.Location = new System.Drawing.Point(18, 134);
            this.buttonIncepe.Margin = new System.Windows.Forms.Padding(2);
            this.buttonIncepe.Name = "buttonIncepe";
            this.buttonIncepe.Size = new System.Drawing.Size(141, 28);
            this.buttonIncepe.TabIndex = 4;
            this.buttonIncepe.Text = "LOGIN";
            this.buttonIncepe.UseVisualStyleBackColor = true;
            this.buttonIncepe.Click += new System.EventHandler(this.buttonIncepe_Click);
            // 
            // textBoxParola2
            // 
            this.textBoxParola2.Location = new System.Drawing.Point(134, 95);
            this.textBoxParola2.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxParola2.Name = "textBoxParola2";
            this.textBoxParola2.PasswordChar = '*';
            this.textBoxParola2.Size = new System.Drawing.Size(120, 20);
            this.textBoxParola2.TabIndex = 3;
            // 
            // textBoxNumeUtil2
            // 
            this.textBoxNumeUtil2.Location = new System.Drawing.Point(134, 64);
            this.textBoxNumeUtil2.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxNumeUtil2.Name = "textBoxNumeUtil2";
            this.textBoxNumeUtil2.Size = new System.Drawing.Size(120, 20);
            this.textBoxNumeUtil2.TabIndex = 2;
            // 
            // labelParola2
            // 
            this.labelParola2.AutoSize = true;
            this.labelParola2.Location = new System.Drawing.Point(15, 95);
            this.labelParola2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelParola2.Name = "labelParola2";
            this.labelParola2.Size = new System.Drawing.Size(50, 13);
            this.labelParola2.TabIndex = 1;
            this.labelParola2.Text = "PAROLA";
            // 
            // labelNumeUtil2
            // 
            this.labelNumeUtil2.AutoSize = true;
            this.labelNumeUtil2.Location = new System.Drawing.Point(15, 64);
            this.labelNumeUtil2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelNumeUtil2.Name = "labelNumeUtil2";
            this.labelNumeUtil2.Size = new System.Drawing.Size(29, 13);
            this.labelNumeUtil2.TabIndex = 0;
            this.labelNumeUtil2.Text = "CNP";
            // 
            // c
            // 
            this.AcceptButton = this.buttonIncepe;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(822, 482);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ButtonAdaugare);
            this.Controls.Add(this.ButtonAutentificare);
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "c";
            this.Text = "Aplicatie Medicala";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.c_FormClosed);
            this.Load += new System.EventHandler(this.c_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ButtonAutentificare;
        private System.Windows.Forms.Button ButtonAdaugare;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label labelNume;
        private System.Windows.Forms.Label labelprenume;
        private System.Windows.Forms.Label labelCNP;
        private System.Windows.Forms.Label labelAdresa;
        private System.Windows.Forms.Label labelTelefon;
        private System.Windows.Forms.TextBox textBoxNume;
        private System.Windows.Forms.TextBox textBoxPrenume;
        private System.Windows.Forms.TextBox textBoxCNP;
        private System.Windows.Forms.TextBox textBoxAdresa;
        private System.Windows.Forms.TextBox textBoxTelefon;
        private System.Windows.Forms.Label labelEmail;
        private System.Windows.Forms.TextBox textBoxEmail;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonInregistrare;
        private System.Windows.Forms.TextBox textBoxParola;
        private System.Windows.Forms.Label labelParola;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonIncepe;
        private System.Windows.Forms.TextBox textBoxParola2;
        private System.Windows.Forms.TextBox textBoxNumeUtil2;
        private System.Windows.Forms.Label labelParola2;
        private System.Windows.Forms.Label labelNumeUtil2;
        private System.Windows.Forms.TextBox textBoxConfirmareP;
        private System.Windows.Forms.Label label1;
    }
}

