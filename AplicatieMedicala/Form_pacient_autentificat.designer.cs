﻿namespace AplicatieMedicala
{
    partial class FormPacient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_FeedBack = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button_Chat = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textboxOras = new System.Windows.Forms.TextBox();
            this.textboxNume = new System.Windows.Forms.TextBox();
            this.labelzona = new System.Windows.Forms.Label();
            this.labelNumeUtil2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // button_FeedBack
            // 
            this.button_FeedBack.Location = new System.Drawing.Point(258, 93);
            this.button_FeedBack.Name = "button_FeedBack";
            this.button_FeedBack.Size = new System.Drawing.Size(75, 23);
            this.button_FeedBack.TabIndex = 1;
            this.button_FeedBack.Text = "Feedback";
            this.button_FeedBack.UseVisualStyleBackColor = true;
            this.button_FeedBack.Click += new System.EventHandler(this.button_FeedBack_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(82, 100);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(120, 46);
            this.button2.TabIndex = 2;
            this.button2.Text = "Cauta Medici";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // button_Chat
            // 
            this.button_Chat.Location = new System.Drawing.Point(258, 122);
            this.button_Chat.Name = "button_Chat";
            this.button_Chat.Size = new System.Drawing.Size(75, 51);
            this.button_Chat.TabIndex = 3;
            this.button_Chat.Text = "Chat ( proba/separat )";
            this.button_Chat.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(142, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Bine ai venit !";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textboxOras);
            this.groupBox2.Controls.Add(this.textboxNume);
            this.groupBox2.Controls.Add(this.labelzona);
            this.groupBox2.Controls.Add(this.labelNumeUtil2);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Location = new System.Drawing.Point(31, 61);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(213, 151);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Cautare Medici";
            // 
            // textboxOras
            // 
            this.textboxOras.Location = new System.Drawing.Point(82, 63);
            this.textboxOras.Margin = new System.Windows.Forms.Padding(2);
            this.textboxOras.Name = "textboxOras";
            this.textboxOras.Size = new System.Drawing.Size(120, 20);
            this.textboxOras.TabIndex = 3;
            // 
            // textboxNume
            // 
            this.textboxNume.Location = new System.Drawing.Point(82, 32);
            this.textboxNume.Margin = new System.Windows.Forms.Padding(2);
            this.textboxNume.Name = "textboxNume";
            this.textboxNume.Size = new System.Drawing.Size(120, 20);
            this.textboxNume.TabIndex = 2;
            // 
            // labelzona
            // 
            this.labelzona.AutoSize = true;
            this.labelzona.Location = new System.Drawing.Point(15, 63);
            this.labelzona.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelzona.Name = "labelzona";
            this.labelzona.Size = new System.Drawing.Size(37, 13);
            this.labelzona.TabIndex = 1;
            this.labelzona.Text = "ORAS";
            // 
            // labelNumeUtil2
            // 
            this.labelNumeUtil2.AutoSize = true;
            this.labelNumeUtil2.Location = new System.Drawing.Point(15, 32);
            this.labelNumeUtil2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelNumeUtil2.Name = "labelNumeUtil2";
            this.labelNumeUtil2.Size = new System.Drawing.Size(39, 13);
            this.labelNumeUtil2.TabIndex = 0;
            this.labelNumeUtil2.Text = "NUME";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(258, 179);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 46);
            this.button1.TabIndex = 20;
            this.button1.Text = "Fisa Pacient";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(258, 39);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 45);
            this.button3.TabIndex = 21;
            this.button3.Text = "Meniu Login";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // FormPacient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(359, 286);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_Chat);
            this.Controls.Add(this.button_FeedBack);
            this.Name = "FormPacient";
            this.Text = "Interfata Pacient";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormPacient_FormClosing);
            this.Load += new System.EventHandler(this.FormPacient_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button_FeedBack;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button_Chat;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textboxOras;
        private System.Windows.Forms.TextBox textboxNume;
        private System.Windows.Forms.Label labelzona;
        private System.Windows.Forms.Label labelNumeUtil2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
    }
}

