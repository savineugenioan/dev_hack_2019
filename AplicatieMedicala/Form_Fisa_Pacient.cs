﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace AplicatieMedicala
{
    public partial class Form_Fisa_Pacient : Form
    {
        public Form_Fisa_Pacient()
        {
            InitializeComponent();
        }
        public OleDbCommand cmd = new OleDbCommand();
        public OleDbConnection cnn = new OleDbConnection();
        public OleDbDataReader reader;

        private void Form_Fisa_Pacient_Load(object sender, EventArgs e)
        {
            Form f = LastForm.last;
            comboBox1.SelectedIndex = 0;
            if (f.GetType() == typeof(Form_medic))
                groupBox2.Visible = true;
            else
                groupBox2.Visible = false;
            string connetionString = null;
            string sql = null;
            connetionString = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + AppDomain.CurrentDomain.BaseDirectory + @"Proiect_Cabinet.accdb";
            sql = @"SELECT PACIENTI.id_client,PACIENTI.nume, PACIENTI.prenume, PACIENTI.CNP, PACIENTI.adresa, PACIENTI.nr_telefon, PACIENTI.email FROM PACIENTI WHERE (((PACIENTI.CNP) Like '"+Global.str1+"'))";
            cnn = new OleDbConnection(connetionString);
            cmd = new OleDbCommand(sql, cnn);
            string id_client= "";
            try
            {
                if (cnn.State == ConnectionState.Closed)
                {
                    cnn.Open();
                }
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    id_client = reader.GetValue(0).ToString();
                    Global.nume = id_client;
                    textBox1.Text = reader.GetValue(1).ToString();
                    textBox2.Text = reader.GetValue(2).ToString();
                    textBox3.Text = reader.GetValue(3).ToString();
                    textBox4.Text = reader.GetValue(4).ToString();
                    textBox5.Text = reader.GetValue(5).ToString();
                    textBox6.Text = reader.GetValue(6).ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            ///lista populare
            sql = @"SELECT CONSULT.id_consult, MEDICAMENTE.nume_medicament FROM ((PACIENTI INNER JOIN CONSULT ON PACIENTI.id_client = CONSULT.id_client) INNER JOIN TRATAMENT ON CONSULT.id_consult = TRATAMENT.id_consult) INNER JOIN (MEDICAMENTE INNER JOIN TRAT_MED ON MEDICAMENTE.id_medicament = TRAT_MED.id_medicament) ON TRATAMENT.id_tratament = TRAT_MED.id_tratament WHERE (((CONSULT.id_client) Like '"+id_client+"'))";
            cnn = new OleDbConnection(connetionString);
            cmd = new OleDbCommand(sql, cnn);
            try
            {
                if (cnn.State == ConnectionState.Closed)
                {
                    cnn.Open();
                }
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    listBox1.Items.Add("ID Control : " + reader.GetValue(0) + ":" + reader.GetValue(1));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            reader.Close();
            cmd.Dispose();
            cnn.Close();
        }

        private void Form_Fisa_Pacient_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form f =LastForm.last;
            f.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string connetionString = null;
            string sql = null;
            string id_doctor="";
            connetionString = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + AppDomain.CurrentDomain.BaseDirectory + @"Proiect_Cabinet.accdb";
            sql = @"SELECT DOCTORI.CNP, DOCTORI.id_doctor FROM DOCTORI";
            cnn = new OleDbConnection(connetionString);
            cmd = new OleDbCommand(sql, cnn);
            try
            {
                if (cnn.State == ConnectionState.Closed)
                {
                    cnn.Open();
                }
                reader = cmd.ExecuteReader();
                string x = Global.str1;
                while (reader.Read())
                {

                    string y = reader.GetValue(0).ToString();
                    if (x == y)
                    {
                        id_doctor = reader.GetValue(1).ToString();
                        break;
                    }
                }
                reader.Close();
                cmd.Dispose();
                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            try
            {
                if (cnn.State == ConnectionState.Closed)
                {
                    cnn.Open();
                }
                sql = @"INSERT INTO CONSULT(id_client, id_doctor, data_consult) Values('" + Global.nume + "'" + "," + "'" + id_doctor + "'" + "," + "'" + dateTimePicker1.Text + "'" + ")";
                cmd = new OleDbCommand(sql, cnn);
                cmd.ExecuteNonQuery();
                reader.Close();
                cmd.Dispose();
                cnn.Close();
            }
            catch(Exception er)
            {
                MessageBox.Show(er.Message);
            }
            int rt = 0;
            try
            {
                if (cnn.State == ConnectionState.Closed)
                {
                    cnn.Open();
                }
                sql = @"SELECT CONSULT.id_consult FROM CONSULT;";
                cmd = new OleDbCommand(sql, cnn);
                reader = cmd.ExecuteReader();
                while(reader.Read())
                {
                    int i = int.Parse(reader.GetValue(0).ToString());
                    if (rt < i)
                      rt = i;
                }
                reader.Close();
                cmd.Dispose();
                cnn.Close();
            }
            
            catch (Exception er)
            {
                MessageBox.Show(er.Message);
            }
            try
            {
                if (cnn.State == ConnectionState.Closed)
                {
                    cnn.Open();
                }
                sql = @"INSERT INTO TRATAMENT(id_consult,incepere_tratament) Values('" + rt + "'" + "," + "'" + dateTimePicker2.Text + "'"+ ")";
                cmd = new OleDbCommand(sql, cnn);
                cmd.ExecuteNonQuery();
                reader.Close();
                cmd.Dispose();
                cnn.Close();
            }
            catch (Exception er)
            {
                MessageBox.Show(er.Message);
            }
            rt = 0;
            try
            {
                if (cnn.State == ConnectionState.Closed)
                {
                    cnn.Open();
                }
                sql = "SELECT TRATAMENT.id_tratament FROM TRATAMENT";
                cmd = new OleDbCommand(sql, cnn);
                reader = cmd.ExecuteReader();
                while(reader.Read())
                {
                    int i = int.Parse(reader.GetValue(0).ToString());
                    if (rt < i)
                        rt = i;
                }
                reader.Close();
                cmd.Dispose();
                cnn.Close();
            }
            catch (Exception er)
            {
                MessageBox.Show(er.Message);
            }
            try
            {
                if (cnn.State == ConnectionState.Closed)
                {
                    cnn.Open();
                }
                int cant = 0;
                cant = int.Parse(textBox7.Text);
                sql = @"INSERT INTO TRAT_MED(id_tratament,id_medicament,cant_prescrisa) Values('" + rt + "'" + "," + "'" + (comboBox1.SelectedIndex+1) + "'" + "," + "'" + cant + "'"+ ")";
                cmd = new OleDbCommand(sql, cnn);
                cmd.ExecuteNonQuery();
                reader.Close();
                cmd.Dispose();
                cnn.Close();
            }
            catch (Exception er)
            {
                MessageBox.Show(er.Message);
            }
            MessageBox.Show("Operatiune Finalizata !");
            textBox7.Text = "";
        }
    }
}
