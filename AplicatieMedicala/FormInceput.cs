﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace AplicatieMedicala
{
    public partial class c : Form
    {
        public OleDbCommand cmd = new OleDbCommand();
        public OleDbConnection cnn = new OleDbConnection();
        public OleDbDataReader reader;
        public c()
        {
            InitializeComponent();
        }
        private void ButtonAutentificare_Click(object sender, EventArgs e)
        {
            groupBox2.Visible = true;
            groupBox1.Visible = false;
            textBoxNumeUtil2.Focus();
        }

        private void ButtonAdaugare_Click(object sender, EventArgs e)
        {
            groupBox2.Visible = false;
            groupBox1.Visible = true;
            textBoxNume.Focus();
        }

        private void buttonInregistrare_Click(object sender, EventArgs e)
        {
            string connetionString = null;
            string sql = null;
            if(textBoxParola.Text!=textBoxConfirmareP.Text)
            {
                MessageBox.Show("Confirmare parola esuata !");
                return;
            }
            if (textBoxCNP.Text.Length == 13 & textBoxNume.Text != null & textBoxPrenume.Text != null & textBoxAdresa.Text != null & textBoxTelefon != null & textBoxParola.Text != null)
            {
                connetionString = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + AppDomain.CurrentDomain.BaseDirectory + @"Proiect_Cabinet.accdb";
                sql = @"SELECT PACIENTI.CNP FROM PACIENTI";
                cnn = new OleDbConnection(connetionString);
                cmd = new OleDbCommand(sql, cnn);
                try
                {
                    if (cnn.State == ConnectionState.Closed)
                    {
                        cnn.Open();
                    }
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        string x = reader.GetValue(0).ToString();
                        if (x == textBoxCNP.Text.ToString())
                        {
                            MessageBox.Show("Utilizator deja inregistrat");
                            return;
                        }
                    }
                    try
                    {
                        sql = @"INSERT into PACIENTI (nume,prenume,adresa,nr_telefon,email,CNP,parola) Values('" + textBoxNume.Text + "'" + "," + "'" + textBoxPrenume.Text + "'" + "," + "'" + textBoxAdresa.Text + "'" + "," + "'" + textBoxTelefon.Text + "'" + "," + "'" + textBoxEmail.Text + "'" + "," + "'" + textBoxCNP.Text + "'" + "," + "'" + textBoxParola.Text + "'" + ")";

                        cmd = new OleDbCommand(sql, cnn);
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    finally
                    {
                        MessageBox.Show("Inregistrare reusita");
                    }
                    reader.Close();
                    cmd.Dispose();
                    cnn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            textBoxNume.Text = String.Empty;
            textBoxPrenume.Text = String.Empty;
            textBoxAdresa.Text = String.Empty;
            textBoxCNP.Text = String.Empty;
            textBoxTelefon.Text = String.Empty;
            textBoxEmail.Text = String.Empty;
            textBoxParola.Text = String.Empty;
            textBoxConfirmareP.Text = String.Empty;
            }
        else MessageBox.Show("Nu ati completat toate datele, sau CNP-ul e gresit");
        }


        private void buttonIncepe_Click(object sender, EventArgs e)
        {
            Global.str1 = textBoxNumeUtil2.Text;
            Global.str2 = textBoxParola2.Text;
            string CNP="", parola="",combo="";
            CNP = textBoxNumeUtil2.Text;
            parola = textBoxParola2.Text;
            combo = comboBox1.Text;
            if (comboBox1.Text != "PACIENT" && comboBox1.Text != "MEDIC")
            {
                MessageBox.Show("Va rugam sa selectati un tip de cont ( Pacient sau MEDIC ) !");
                return;
            }
            string connetionString = null;
            string sql = null;
            connetionString = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + AppDomain.CurrentDomain.BaseDirectory + @"Proiect_Cabinet.accdb";
            cnn = new OleDbConnection(connetionString);
            try
            {
                if (cnn.State == ConnectionState.Closed)
                {
                    cnn.Open();
                }
                if ((CNP == "") || (parola == ""))
                    MessageBox.Show("Va rog sa completati toate campurile");
                else
                {
                    if (combo == "PACIENT")///
                    {
                        sql = @"SELECT PACIENTI.CNP, PACIENTI.parola FROM PACIENTI";
                    }
                    else if (combo == "MEDIC")///
                    {
                        sql = @"SELECT DOCTORI.CNP, DOCTORI.parola FROM DOCTORI";
                    }
                    cmd = new OleDbCommand(sql, cnn);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        string x = reader.GetValue(0).ToString();
                        string y = reader.GetValue(1).ToString();
                        if (x == CNP && y==parola)
                        {
                            reader.Close();
                            cmd.Dispose();
                            cnn.Close();
                            this.Hide();
                            if (combo == "PACIENT")
                            {
                                Form f = new FormPacient();
                                f.Show();
                            }
                            else if (combo == "MEDIC")
                            {
                                Form f = new Form_medic();
                                f.Show();
                            }
                            return;
                        }
                        else if(x==CNP)
                        {
                            MessageBox.Show("Parola Gresita !!");
                            return;
                        }
                    }
                    MessageBox.Show("Utilizatorul nu a fost gasit in baza de date !");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void c_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedItem = "PACIENT";
            ButtonAutentificare_Click(sender,e);
        }
        private void c_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
    public class Global
    {
        public static string str1, str2,nume;
        public static List<Medici> medici = new List<Medici>();
    }
}
